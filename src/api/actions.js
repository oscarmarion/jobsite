import api from './api';

export const getHomepage = async () => {
  return await api.get('/home-page');
};

export const getQuote = async (id) => {
  return await api.get(`/quote/${id}`);
};

export const getHomepageBlock = async (id) => {
  return await api.get(`/home-page-block/${id}`);
};

export const getEvents = async () => {
  return await api.get('/events');
};
