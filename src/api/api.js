import axios from 'axios';

export default axios.create({
  baseURL: `${process.env.REACT_APP_API}/api`,
  headers: { Authorization: 'Basic ZGVmYXVsdF9hcGlfdXNlcjpVaTdvaHhhNW9obTM=' },
});
