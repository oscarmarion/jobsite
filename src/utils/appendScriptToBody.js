const appendScriptToBody = (src, async = true, defer = false) => {
  const script = document.createElement('script');
  script.src = src;
  script.type = 'text/javascript';
  script.async = async;
  script.defer = defer;
  document.body.appendChild(script);
};

export default appendScriptToBody;
