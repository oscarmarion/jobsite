import React from 'react';
import { Router } from '@reach/router';

import './App.module.scss';

// Pages
import { Home, Jobs, LifeAtCap, Events, Apply } from './pages';

// Components
import Header from './pages/shared/Header';
import Footer from './pages/shared/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <Router>
        <Home path="/" />
        <Jobs path="/jobs" />
        <LifeAtCap path="/life-at-capgemini" />
        <Apply path="/apply" />
        <Events path="/events" />
      </Router>
      <Footer />
    </div>
  );
}

export default App;
