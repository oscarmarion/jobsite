import React, { Component } from 'react';
import PropTypes from 'prop-types';
import appendScriptToBody from '../../../utils/appendScriptToBody';

class JobylonWidget extends Component {
  static defaultProps = {
    company_id: 1760,
    version: 'v2',
    page_size: 10,
  };
  _setJobylonConfig(company_id, version, pageSize) {
    window.jbl_company_id = company_id;
    window.jbl_version = version;
    window.jbl_page_size = pageSize;
  }
  componentDidMount() {
    const { company_id, version, pageSize } = this.props;
    this._setJobylonConfig(company_id, version, pageSize);
    appendScriptToBody('https://cdn.jobylon.com/embedder.js');
  }
  render() {
    return <div id="jobylon-jobs-widget"></div>;
  }
}
JobylonWidget.propTypes = {
  company_id: PropTypes.number,
  version: PropTypes.string,
  page_size: PropTypes.number,
};
export default JobylonWidget;
