import React from 'react';
import { Link } from '@reach/router';

const Header = () => {
  return (
    <header>
      <p>Header</p>
      <Link to="/">Home</Link>
      <Link to="/jobs">Jobs</Link>
      <Link to="/life-at-capgemini">Life at Cap</Link>
      <Link to="/events">Events</Link>
      <Link to="/apply">Apply</Link>
    </header>
  );
};

export default Header;
