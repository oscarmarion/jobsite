export { default as Home } from './home/Home';
export { default as Jobs } from './jobs/Jobs';
export { default as LifeAtCap } from './life-at-cap/LifeAtCap';
export { default as Apply } from './apply/Apply';
export { default as Events } from './events/Events';
